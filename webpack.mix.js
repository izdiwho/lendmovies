let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js'
    ], 'public/js/datatables.js')
    .styles([
        'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css'
    ], 'public/css/datatables.css')
    .scripts([
        'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
    ], 'public/js/sweetalert2.js')
    .styles([
        'node_modules/sweetalert2/dist/sweetalert2.all.min.css',
    ], 'public/css/sweetalert2.css')
    .styles([
        'resources/assets/css/bootstrap-multiselect.css'
    ], 'public/css/multiselect.css')
    .scripts([
        'resources/assets/js/bootstrap-multiselect.js',
    ], 'public/js/multiselect.js');