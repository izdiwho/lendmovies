<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'name', 'age', 'address', 'telephone', 'icnumber', 'date_joined', 'is_active'
    ];

    public function lendings()
    {
        return $this->hasMany(Lending::class);
    }

    public function getDateJoinedAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function getIsActiveAttribute($value)
    {
        return $value ? 'Yes' : 'No';
    }
}
