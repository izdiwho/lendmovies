<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'title', 'genre', 'released_date', 'is_available'
    ];

    public function lending()
    {
        return $this->hasOne(Lending::class);
    }

    public function getReleasedDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function getIsAvailableAttribute($value)
    {
        return $value ? 'Yes' : 'No';
    }

    public function lent()
    {
        return $this->update([
            'is_available' => 0
        ]);
    }

    public function returned()
    {
        return $this->update([
            'is_available' => 1
        ]);
    }

}
