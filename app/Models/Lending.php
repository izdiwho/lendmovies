<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Lending extends Model
{
    protected $fillable = [
        'movie_id', 'member_id', 'lending_date', 'returned_date', 'late_charges'
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function getLendingDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function getReturnedDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }
}
