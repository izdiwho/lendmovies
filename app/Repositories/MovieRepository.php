<?php

namespace App\Repositories;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Exception;

class MovieRepository
{
    public function getAll()
    {
        return Movie::all();
    }

    public function store($request)
    {
        try {
            Movie::create([
                'title' => $request['title'],
                'genre' => $request['genre'],
                'released_date' => $request['released_date']
            ]);

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function update($request, $movie)
    {
        try {
            $movie->title = $request['title'];
            $movie->genre = $request['genre'];
            $movie->released_date = $request['released_date'];

            $movie->save();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function destroy($movie)
    {
        try {
            $movie->delete();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }
}