<?php

namespace App\Repositories;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Exception;

class MemberRepository
{
    public function getAll()
    {
        return Member::with('lendings', 'lendings.movie')->get();
    }

    public function getActive()
    {
        return Member::where('is_active', true)->get();
    }

    public function store($request)
    {
        try {
            Member::create([
                'name' => $request['name'],
                'age' => $request['age'],
                'address' => $request['address'],
                'telephone' => $request['telephone'],
                'icnumber' => $request['icnumber'],
                'date_joined' => $request['date_joined'],
                'is_active' => $request['is_active']
            ]);

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function update($request, $member)
    {
        try {
            $member->name = $request['name'];
            $member->age = $request['age'];
            $member->address = $request['address'];
            $member->telephone = $request['telephone'];
            $member->icnumber = $request['icnumber'];
            $member->date_joined = $request['date_joined'];
            $member->is_active = $request['is_active'];

            $member->save();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function destroy($member)
    {
        try {
            $member->delete();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }
}