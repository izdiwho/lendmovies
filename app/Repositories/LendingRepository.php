<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Models\Lending;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Exception;

class LendingRepository
{
    public function getAvailable()
    {
        return Movie::where('is_available', true)->get();
    }

    public function getLent()
    {
        return Movie::where('is_available', false)->with(['lending' => function($query){
            $query->orderByDesc('id');
        }, 'lending.movie'])->get();
    }

    public function store($request)
    {
        try {
            Lending::create([
                'movie_id' => $request['movie_id'],
                'member_id' => $request['member_id'],
                'lending_date' => $request['lending_date']
            ]);

            Movie::find($request['movie_id'])->lent();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function update($request, $lending)
    {
        try {
            $lending->returned_date = $request['returned_date'];
            $lending->late_charges = $request['late_charges'];

            $lending->save();

            $lending->movie->returned();

            return true;
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }
}