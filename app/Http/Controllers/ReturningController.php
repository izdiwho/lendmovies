<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Lending;
use Illuminate\Http\Request;
use App\Repositories\LendingRepository;
use App\Repositories\MemberRepository;

class ReturningController extends Controller
{

    private $lending, $member;

    public function __construct(LendingRepository $lending, MemberRepository $member)
    {
        $this->lending = $lending;
        $this->member = $member;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = $this->lending->getLent();

        return view('returning', [
            'movies' => $movies
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lending $lending)
    {
        $validator = Validator::make($request->all(), [
            'returned_date' => 'required',
            'late_charges' => 'required'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Movie return failed!', 'error');

            return back()->withErrors($validator, 'update_movie')
                        ->withInput()
                        ->with('id', $lending->id);
        }

        if ($this->lending->update($request, $lending)) {
            flash('Success!', 'Movie successfully returned!', 'success');

            return back();
        }

        flash('Error!', 'Movie return failed!', 'error');

        return back();
    }
}
