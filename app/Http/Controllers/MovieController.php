<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use App\Repositories\MovieRepository;
use Validator;

class MovieController extends Controller
{

    private $movie;

    public function __construct(MovieRepository $movie)
    {
        $this->movie = $movie;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = $this->movie->getAll();

        return view('movies.index', [
            'movies' => $movies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre' => 'required|in:Action,Horror,Thriller,Romance',
            'released_date' => 'required'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Movie creation failed!', 'error');

            return back()->withErrors($validator, 'store_movie')
                        ->withInput();
        }

        if ($this->movie->store($request)) {
            flash('Success!', 'Movie successfully created!', 'success');

            return back();
        }

        flash('Error!', 'Movie creation failed!', 'error');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre' => 'required',
            'released_date' => 'required'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Movie update failed!', 'error');

            return back()->withErrors($validator, 'update_movie')
                        ->withInput()
                        ->with('id', $movie->id);
        }

        if ($this->movie->update($request, $movie)) {
            flash('Success!', 'Movie successfully updated!', 'success');

            return back();
        }

        flash('Error!', 'Movie update failed!', 'error');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        if ($this->movie->destroy($movie)) {
            flash('Success!', 'Movie successfully deleted!', 'success');

            return back();
        }
    }
}
