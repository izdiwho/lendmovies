<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Repositories\LendingRepository;
use App\Repositories\MemberRepository;

class LendingController extends Controller
{

    private $lending, $member;

    public function __construct(LendingRepository $lending, MemberRepository $member)
    {
        $this->lending = $lending;
        $this->member = $member;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Available Movies
        $movies = $this->lending->getAvailable();

        // Get members
        $members = $this->member->getActive();

        return view('lending', [
            'movies' => $movies,
            'members' => $members
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'movie_id' => 'required',
            'member_id' => 'required',
            'lending_date' => 'required'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Movie lending failed!', 'error');

            return back()->withErrors($validator, 'store_lending')
                        ->withInput()
                        ->with('id', $request['movie_id']);
        }

        if ($this->lending->store($request)) {
            flash('Success!', 'Movie successfully lended!', 'success');

            return back();
        }

        flash('Error!', 'Movie lending failed!', 'error');

        return back();
    }

}
