<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Repositories\MemberRepository;

class MemberController extends Controller
{

    private $member;

    public function __construct(MemberRepository $member)
    {
        $this->member = $member;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = $this->member->getAll();

        return view('members.index', [
            'members' => $members
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required',
            'address' => 'required',
            'telephone' => 'required',
            'icnumber' => 'required',
            'date_joined' => 'required',
            'is_active' => 'nullable'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Member creation failed!', 'error');

            return back()->withErrors($validator, 'store_member')
                        ->withInput();
        }

        $request['is_active'] = $request->has('is_active') ? true : false;

        if ($this->member->store($request)) {
            flash('Success!', 'Member successfully created!', 'success');

            return back();
        }

        flash('Error!', 'Member creation failed!', 'error');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required',
            'address' => 'required',
            'telephone' => 'required',
            'icnumber' => 'required',
            'date_joined' => 'required',
            'is_active' => 'nullable'
        ]);

        if ($validator->fails()) {
            flash('Error!', 'Member update failed!', 'error');

            return back()->withErrors($validator, 'update_member')
                        ->withInput()
                        ->with('id', $member->id);
        }

        $request['is_active'] = $request->has('is_active') ? true : false;

        if ($this->member->update($request, $member)) {
            flash('Success!', 'Member successfully updated!', 'success');

            return back();
        }

        flash('Error!', 'Member update failed!', 'error');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        if ($this->member->destroy($member)) {
            flash('Success!', 'Member successfully deleted!', 'success');

            return back();
        }
    }
}
