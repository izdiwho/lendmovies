<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Member;
use App\Models\Movie;

class AllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(User::class)->create([
            'name' => 'Administrator',
            'email' => 'admin@test.com'
        ]);


        factory(Member::class, 15)->create();
        factory(Movie::class, 15)->create();
    }
}
