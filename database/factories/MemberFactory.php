<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'age' => $faker->numberBetween(16,25),
        'address' => $faker->address,
        'telephone' => $faker->phoneNumber,
        'icnumber' => $faker->numberBetween(11010101,19090909),
        'date_joined' => $faker->dateTimeBetween('-10 days', 'now')
    ];
});
