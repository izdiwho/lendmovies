<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'genre' => $faker->randomElement(['Action', 'Horror', 'Thriller', 'Romance']),
        'released_date' => $faker->dateTimeBetween('-10 years', 'now')
    ];
});
