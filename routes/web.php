<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/movies', 'MovieController@index')->name('movies');
Route::post('/movies', 'MovieController@store')->name('movies.store');
Route::put('/movies/{movie}', 'MovieController@update')->name('movies.update');
Route::delete('/movies/{movie}', 'MovieController@destroy')->name('movies.destroy');
Route::get('/members', 'MemberController@index')->name('members');
Route::post('/members', 'MemberController@store')->name('members.store');
Route::put('/members/{member}', 'MemberController@update')->name('members.update');
Route::delete('/members/{member}', 'MemberController@destroy')->name('members.destroy');

Route::get('/lending', 'LendingController@index')->name('lending');
Route::post('/lending', 'LendingController@store')->name('lending.store');

Route::get('/returning', 'ReturningController@index')->name('returning');
Route::put('/returning/{lending}', 'ReturningController@update')->name('returning.update');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
