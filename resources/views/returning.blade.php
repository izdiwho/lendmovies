@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Movies Lended
                    </div>
                    <div class="card-body">
                        <table id="movies" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Genre</th>
                                    <th>Released Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($movies as $movie)
                                    <tr>
                                        <td>{{ $movie->title }}</td>
                                        <td>{{ $movie->genre }}</td>
                                        <td>{{ $movie->released_date }}</td>
                                        <td>
                                            <button
                                            id="returnMovieBtn"
                                            class="btn btn-sm btn-block"
                                            data-toggle="modal"
                                            data-target="#returnMovieModal"
                                            data-action="{{ route('returning.update', ['lending' => $movie->lending->id]) }}"
                                            data-id="{{ $movie->lending->id }}"
                                            data-title="{{ $movie->title }}"
                                            data-member-name="{{ $movie->lending->member->name }}"
                                            data-lending-date="{{ $movie->lending->lending_date }}">
                                            Return</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('modals')

    {{-- Lend Movie Modal Component --}}
    @component('components.modal')
        @slot('id') returnMovieModal @endslot
        @slot('title') Return this Movie @endslot
        @slot('body')
        <form id="return_form" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group row">
                <label for="movie_title" class="col-md-4 col-form-label text-md-right">Movie Title</label>

                <div class="col-md-6">
                    <input id="movie_title" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="member_name" class="col-md-4 col-form-label text-md-right">Lent To</label>

                <div class="col-md-6">
                    <input id="member_name" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="lending_date" class="col-md-4 col-form-label text-md-right">Lent Since</label>

                <div class="col-md-6">
                    <input id="lending_date" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="returned_date" class="col-md-4 col-form-label text-md-right">Returned Date</label>

                <div class="col-md-6">
                    <input id="returned_date" type="date" class="form-control{{ $errors->store_lending->has('returned_date') ? ' is-invalid' : '' }}" name="returned_date" value="{{ old('returned_date') }}" placeholder="Returned Date" required>
                    @if ($errors->store_lending->has('returned_date'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->store_lending->first('returned_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="late_charges" class="col-md-4 col-form-label text-md-right">Late Charges</label>

                <div class="col-md-6">
                    <input id="late_charges" type="text" class="form-control" name="late_charges" readonly>
                </div>
            </div>

            <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="submitBtn">Submit</button>

        </form>
        @endslot
        @slot('footer') @endslot

    @endcomponent

@endpush

@push('styles')
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/sweetalert2.css">
    <link rel="stylesheet" href="css/multiselect.css">
@endpush

@push('scripts')
    <script src="js/datatables.js"></script>
    <script src="js/sweetalert2.js"></script>
    <script src="js/multiselect.js"></script>
    <script>
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = date1_ms - date2_ms

            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }

        $(document).ready(function () {
            // Initialize movies datatable
            $('#movies').DataTable();

            // Sweet alert component
            @component('components.sw2')
                @slot('title')
                    {!! session('title') !!}
                @endslot
                @slot('description')
                    {!! session('description') !!}
                @endslot
                @slot('status')
                    {!! session('status') !!}
                @endslot
            @endcomponent

            $(document).on("click", "#returnMovieBtn", function () {
                var action = $(this).data('action');
                var movie_title = $(this).data('title');
                var member_name = $(this).data('member-name');
                var lending_date = $(this).data('lending-date');

                $('#return_form').attr('action', action);
                $('#movie_title').val(movie_title);
                $('#member_name').val(member_name);
                $('#lending_date').val(lending_date);
                $('#late_charges').val('0.00');

                $(document).on("change", "#returned_date", function () {
                    $('#late_charges').val('0.00');
                    var late_charge_per_day = 0.5; // change value accordingly
                    var late_max_duration = 7;
                    var returned_date = new Date($(this).val());
                    var days_since = days_between(returned_date, new Date(lending_date));
                    if (days_since > late_max_duration) {
                        var late_days = days_since - late_max_duration;
                        var late_charges = late_days * late_charge_per_day;
                        $('#late_charges').val(late_charges.toFixed(2));
                    }
                });

                var confirmed = false;
                $(document).on("submit", "#return_form", function (e) {
                    if (!confirmed) {
                        e.preventDefault(e);
                        swal({
                            title: 'Are you sure?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                confirmed = true;
                                $('#return_form').submit();
                            }
                        });
                    }
                });
            });

            @if (count($errors->store_lending) > 0)
                setTimeout(function(){
                    $('#returnMovieBtn[data-id="{{ session('id') }}"]').click();
                }, 500);
            @endif
        });
    </script>
@endpush