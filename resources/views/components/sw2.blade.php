@if (session('status'))
    Swal(
        '{{ $title }}',
        '{{ $description }}',
        '{{ $status }}'
        );
@endif