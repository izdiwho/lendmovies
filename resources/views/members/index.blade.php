@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Members
                    <button id="addMemberBtn" class="btn btn-sm btn-success float-right" data-toggle="modal"
                        data-target="#addNewMemberModal">Add New Member</button>
                </div>
                <div class="card-body">
                    <table id="members" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Date Joined</th>
                                <th>Active</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($members as $member)
                            <tr>
                                <td>{{ $member->name }}</td>
                                <td>{{ $member->age }}</td>
                                <td>{{ $member->date_joined }}</td>
                                <td>{{ $member->is_active }}</td>
                                <td>
                                    <button id="viewMemberBtn" class="btn btn-sm" data-toggle="modal" data-target="#viewMemberModal"
                                        data-name="{{ $member->name }}" data-age="{{ $member->age }}" data-address="{{ $member->address }}"
                                        data-telephone="{{ $member->telephone }}" data-icnumber="{{ $member->icnumber }}"
                                        data-date-joined="{{ $member->date_joined }}" data-is-active="{{ $member->is_active }}"
                                        data-lendings="{{ $member->lendings }}">
                                        View</button>
                                    <button id="editMemberBtn" class="btn btn-sm" data-toggle="modal" data-target="#editMemberModal"
                                        data-action="{{ route('members.update', ['member' => $member->id]) }}" data-id="{{ $member->id }}"
                                        data-name="{{ $member->name }}" data-age="{{ $member->age }}" data-address="{{ $member->address }}"
                                        data-telephone="{{ $member->telephone }}" data-icnumber="{{ $member->icnumber }}"
                                        data-date-joined="{{ $member->date_joined }}" data-is-active="{{ $member->is_active }}">
                                        Edit</button>
                                    <button id="deleteMemberBtn" class="btn btn-sm" data-action="{{ route('members.destroy', ['member' => $member->id]) }}">
                                        Delete</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('modals')
{{-- Add Modal Component --}}
@component('components.modal')
@slot('id') addNewMemberModal @endslot
@slot('title') Add New Member @endslot
@slot('body')
<form action="{{ route('members.store') }}" method="POST">
    {{ csrf_field() }}

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->store_movie->has('name') ? ' is-invalid' : '' }}"
                name="name" value="{{ old('name') }}" placeholder="Name" required>
            @if ($errors->store_member->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('name') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="icnumber" class="col-md-4 col-form-label text-md-right">IC Number</label>

        <div class="col-md-6">
            <input id="icnumber" type="text" class="form-control{{ $errors->store_member->has('icnumber') ? ' is-invalid' : '' }}"
                name="icnumber" value="{{ old('icnumber') }}" placeholder="IC Number" required>
            @if ($errors->store_member->has('icnumber'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('icnumber') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="age" class="col-md-4 col-form-label text-md-right">Age</label>

        <div class="col-md-6">
            <input id="age" type="number" class="form-control{{ $errors->store_member->has('age') ? ' is-invalid' : '' }}"
                name="age" value="{{ old('age') }}" placeholder="Age" required>
            @if ($errors->store_member->has('age'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('age') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

        <div class="col-md-6">
            <textarea id="address" class="form-control{{ $errors->store_member->has('address') ? ' is-invalid' : '' }}"
                name="address" required>{{ old('address') }}</textarea>
            @if ($errors->store_member->has('address'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('address') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="telephone" class="col-md-4 col-form-label text-md-right">Telephone</label>

        <div class="col-md-6">
            <input id="telephone" type="text" class="form-control{{ $errors->store_member->has('telephone') ? ' is-invalid' : '' }}"
                name="telephone" value="{{ old('telephone') }}" placeholder="Telephone" required>
            @if ($errors->store_member->has('telephone'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('telephone') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="date_joined" class="col-md-4 col-form-label text-md-right">Date Joined</label>

        <div class="col-md-6">
            <input id="date_joined" type="date" class="form-control{{ $errors->store_member->has('date_joined') ? ' is-invalid' : '' }}"
                name="date_joined" value="{{ old('date_joined') }}" placeholder="Date Joined" required>
            @if ($errors->store_member->has('date_joined'))
            <span class="invalid-feedback">
                <strong>{{ $errors->store_member->first('date_joined') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-4 form-check-label text-md-right" for="is_active">Active?</label>

        <div class="col-md-6">
            <input type="checkbox" id="is_active" name="is_active" value="1">
        </div>
    </div>

    <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="saveBtn">Save</button>

</form>
@endslot
@slot('footer') @endslot

@endcomponent

{{-- View Modal Component --}}
@component('components.modal')
@slot('id') viewMemberModal @endslot
@slot('title')
View Member
<button id="lendingRecordsBtn" class="btn btn-sm" data-toggle="modal" data-target="#lendingRecordsModal">
    Lending Records</button>
@endslot
@slot('body')
<div>
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

        <div class="col-md-6">
            <input id="view_name" type="text" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="icnumber" class="col-md-4 col-form-label text-md-right">IC Number</label>

        <div class="col-md-6">
            <input id="view_icnumber" type="text" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="age" class="col-md-4 col-form-label text-md-right">Age</label>

        <div class="col-md-6">
            <input id="view_age" type="text" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

        <div class="col-md-6">
            <textarea id="view_address" type="text" class="form-control" readonly></textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for="telephone" class="col-md-4 col-form-label text-md-right">Telephone</label>

        <div class="col-md-6">
            <input id="view_telephone" type="text" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="date_joined" class="col-md-4 col-form-label text-md-right">Date Joined</label>

        <div class="col-md-6">
            <input id="view_date_joined" type="text" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="is_active" class="col-md-4 col-form-label text-md-right">Active</label>

        <div class="col-md-6">
            <input id="view_is_active" type="text" class="form-control" readonly>
        </div>
    </div>

</div>
@endslot
@slot('footer') @endslot

@endcomponent

{{-- Edit Modal Component --}}
@component('components.modal')
@slot('id') editMemberModal @endslot
@slot('title') Edit Member @endslot
@slot('body')
<form id="edit_form" method="POST">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input id="edit_id" type="hidden" name="id">

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

        <div class="col-md-6">
            <input id="edit_name" type="text" class="form-control{{ $errors->store_movie->has('name') ? ' is-invalid' : '' }}"
                name="name" value="{{ old('name') }}" placeholder="Name" required>
            @if ($errors->update_member->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('name') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="icnumber" class="col-md-4 col-form-label text-md-right">IC Number</label>

        <div class="col-md-6">
            <input id="edit_icnumber" type="text" class="form-control{{ $errors->update_member->has('icnumber') ? ' is-invalid' : '' }}"
                name="icnumber" value="{{ old('icnumber') }}" placeholder="IC Number" required>
            @if ($errors->update_member->has('icnumber'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('icnumber') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="age" class="col-md-4 col-form-label text-md-right">Age</label>

        <div class="col-md-6">
            <input id="edit_age" type="number" class="form-control{{ $errors->update_member->has('age') ? ' is-invalid' : '' }}"
                name="age" value="{{ old('age') }}" placeholder="Age" required>
            @if ($errors->update_member->has('age'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('age') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

        <div class="col-md-6">
            <textarea id="edit_address" class="form-control{{ $errors->update_member->has('address') ? ' is-invalid' : '' }}"
                name="address" required>{{ old('address') }}</textarea>
            @if ($errors->update_member->has('address'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('address') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="telephone" class="col-md-4 col-form-label text-md-right">Telephone</label>

        <div class="col-md-6">
            <input id="edit_telephone" type="text" class="form-control{{ $errors->update_member->has('telephone') ? ' is-invalid' : '' }}"
                name="telephone" value="{{ old('telephone') }}" placeholder="Telephone" required>
            @if ($errors->update_member->has('telephone'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('telephone') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="date_joined" class="col-md-4 col-form-label text-md-right">Date Joined</label>

        <div class="col-md-6">
            <input id="edit_date_joined" type="date" class="form-control{{ $errors->update_member->has('date_joined') ? ' is-invalid' : '' }}"
                name="date_joined" value="{{ old('date_joined') }}" placeholder="Date Joined" required>
            @if ($errors->update_member->has('date_joined'))
            <span class="invalid-feedback">
                <strong>{{ $errors->update_member->first('date_joined') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-4 form-check-label text-md-right" for="is_active">Active?</label>

        <div class="col-md-6">
            <input type="checkbox" id="edit_is_active" name="is_active" value="1">
        </div>
    </div>

    <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="saveBtn">Save</button>

</form>
@endslot
@slot('footer') @endslot

@endcomponent

{{-- Lending History Modal Component --}}
@component('components.modal')
@slot('id') lendingRecordsModal @endslot
@slot('title') Lending Records @endslot
@slot('body')


<div>
    <table id="lendingsTable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Genre</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@endslot
@slot('footer') @endslot

@endcomponent
@endpush

@push('styles')
<link rel="stylesheet" href="css/datatables.css">
<link rel="stylesheet" href="css/sweetalert2.css">
@endpush

@push('scripts')
<script src="js/datatables.js"></script>
<script src="js/sweetalert2.js"></script>
<script>
    function days_between(date1, date2) {

        // The number of milliseconds in one day
        var ONE_DAY = 1000 * 60 * 60 * 24

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime()
        var date2_ms = date2.getTime()

        // Calculate the difference in milliseconds
        var difference_ms = date1_ms - date2_ms

        // Convert back to days and return
        return Math.round(difference_ms / ONE_DAY)

    }

    // Compute status and late charges
    function computeResults(lending_date, returned_date) {
        var lending_date = lending_date ? new Date(lending_date) : null;
        var returned_date = returned_date ? new Date(returned_date) : null;

        var late_charge_per_day = 0.5; // change value accordingly
        var late_max_duration = 7;
        var days = 0;

        var status = "Active";
        var charges = 0.00;
        var results = [];

        if (returned_date) {
            days = days_between(returned_date, lending_date);
            if (days > late_max_duration) {
                status = "Returned but Late";
                charges = late_charge_per_day * days;
                results[0] = status;
                results[1] = charges.toFixed(2);
                return results;
            }
            status = "Returned";
            results[0] = status;
            results[1] = charges.toFixed(2);
            return results;
        }
        days = days_between(new Date(), lending_date);

        if (days > late_max_duration) {
            status = "Late";
            charges = late_charge_per_day * days;
            results[0] = status;
            results[1] = charges.toFixed(2);
            return results;
        }

        results[0] = status;
        results[1] = charges.toFixed(2);
        return results;
    }

    $(document).ready(function () {
        // Initialize movies datatable
        $('#members').DataTable();
        var lendingDataTable = $('#lendingsTable').DataTable({
            "pageLength": 10
        });

        // Sweet alert component
        @component('components.sw2')
        @slot('title') {
            !!session('title') !!
        }
        @endslot
        @slot('description') {
            !!session('description') !!
        }
        @endslot
        @slot('status') {
            !!session('status') !!
        }
        @endslot
        @endcomponent

        $(document).on("click", "#viewMemberBtn", function () {
            var name = $(this).data('name');
            var age = $(this).data('age');
            var address = $(this).data('address');
            var telephone = $(this).data('telephone');
            var icnumber = $(this).data('icnumber');
            var date_joined = $(this).data('date-joined');
            var is_active = $(this).data('is-active');
            var lendings = $(this).data('lendings');

            $('#view_name').val(name);
            $('#view_age').val(age);
            $('#view_address').val(address);
            $('#view_telephone').val(telephone);
            $('#view_icnumber').val(icnumber);
            $('#view_date_joined').val(date_joined);
            $('#view_is_active').val(is_active);
            $('#lendingRecordsBtn').attr('data-lendings', JSON.stringify(lendings));
        });

        $(document).on("click", "#editMemberBtn", function () {
            var action = $(this).data('action');
            var name = $(this).data('name');
            var age = $(this).data('age');
            var address = $(this).data('address');
            var telephone = $(this).data('telephone');
            var icnumber = $(this).data('icnumber');
            var date_joined = $(this).data('date-joined');
            var is_active = $(this).data('is-active');

            $('#edit_form').attr('action', action);
            $('#edit_name').val(name);
            $('#edit_age').val(age);
            $('#edit_address').val(address);
            $('#edit_telephone').val(telephone);
            $('#edit_icnumber').val(icnumber);
            $('#edit_date_joined').val(date_joined);
            if (is_active == 'Yes') {
                $('#edit_is_active').attr('checked', true);
            }
        });

        $(document).on("click", "#deleteMemberBtn", function () {
            var delCtx = $(this);
            var action = delCtx.data('action');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                html: "<form id='delForm' method='POST' action='" + action + "'>" +
                    "<input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]')
                    .attr('content') + "'>" +
                    "<input type='hidden' name='_method' value='DELETE'>" +
                    "</form>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $('#delForm').submit();
                }
            });
        });

        $(document).on("click", "#lendingRecordsBtn", function () {
            lendingDataTable.clear().draw();
            var lendings = $(this).data('lendings');
            lendings.forEach(function (item, index, arr) {
                var title = lendings[index].movie.title;
                var genre = lendings[index].movie.genre;
                var available = lendings[index].movie.is_available;
                var lending_date = lendings[index].lending_date;
                var returned_date = lendings[index].returned_date;
                var results = computeResults(lending_date, returned_date);
                var status = results[0];
                var charges = results[1];

                var content = "Lending Date: " + lending_date + "<br>" +
                    "Returned Date: " + returned_date + "<br>" +
                    "Late Charges: $" + charges;

                var statusPop =
                    '<a href="#" data-trigger="hover" data-toggle="popover" data-placement="left" data-content="' +
                    content + '">' +
                    status + '</a>' +
                    '<sc' + 'ript>$("[data-toggle=\'popover\']").popover({html:true});</sc' +
                    'ript>';

                lendingDataTable.row.add([
                    title,
                    genre,
                    statusPop
                ]).draw(true);
            });

        });

        @if(count($errors->store_member) > 0)
        $('#addMemberBtn').click();
        @endif

        @if(count($errors->update_member) > 0)
        setTimeout(function () {
            $('#editMemberBtn[data-id="{{ session('
                id ') }}"]').click();
        }, 500);
        @endif


    });
</script>
@endpush