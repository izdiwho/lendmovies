@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Movies
                        <button id="addMovieBtn" class="btn btn-sm btn-success float-right" data-toggle="modal" data-target="#addNewMovieModal">Add New Movie</button>
                    </div>
                    <div class="card-body">
                        <table id="movies" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Genre</th>
                                    <th>Release Date</th>
                                    <th>Available</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($movies as $movie)
                                    <tr>
                                        <td>{{ $movie->title }}</td>
                                        <td>{{ $movie->genre }}</td>
                                        <td>{{ $movie->released_date }}</td>
                                        <td>{{ $movie->is_available }}</td>
                                        <td>
                                            <button
                                            id="viewMovieBtn"
                                            class="btn btn-sm"
                                            data-toggle="modal"
                                            data-target="#viewMovieModal"
                                            data-title="{{ $movie->title }}"
                                            data-genre="{{ $movie->genre }}"
                                            data-released-date="{{ $movie->released_date }}">
                                            View</button>
                                            <button
                                            id="editMovieBtn"
                                            class="btn btn-sm"
                                            data-toggle="modal"
                                            data-target="#editMovieModal"
                                            data-action="{{ route('movies.update', ['movie' => $movie->id]) }}"
                                            data-id="{{ $movie->id }}"
                                            data-title="{{ $movie->title }}"
                                            data-genre="{{ $movie->genre }}"
                                            data-released-date="{{ $movie->released_date }}">
                                            Edit</button>
                                            <button
                                            id="deleteMovieBtn"
                                            class="btn btn-sm"
                                            data-action="{{ route('movies.destroy', ['movie' => $movie->id]) }}">
                                            Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('modals')

    {{-- Add Modal Component --}}
    @component('components.modal')
        @slot('id') addNewMovieModal @endslot
        @slot('title') Add New Movie @endslot
        @slot('body')
        <form action="{{ route('movies.store') }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>

                <div class="col-md-6">
                    <input id="title" type="text" class="form-control{{ $errors->store_movie->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" placeholder="Title" required>
                    @if ($errors->store_movie->has('title'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->store_movie->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="genre" class="col-md-4 col-form-label text-md-right">Genre</label>

                <div class="col-md-6">
                    <select id="genre" type="text" class="form-control{{ $errors->store_movie->has('genre') ? ' is-invalid' : '' }}" name="genre" value="{{ old('genre') }}" required>
                        <option value="Action">Action</option>
                        <option value="Horror">Horror</option>
                        <option value="Thriller">Thriller</option>
                        <option value="Romance">Romance</option>
                    </select>
                    @if ($errors->store_movie->has('genre'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->store_movie->first('genre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="released_date" class="col-md-4 col-form-label text-md-right">Released Date</label>

                <div class="col-md-6">
                    <input id="released_date" type="date" class="form-control{{ $errors->store_movie->has('released_date') ? ' is-invalid' : '' }}" name="released_date" value="{{ old('released_date') }}" placeholder="Released Date" required>
                    @if ($errors->store_movie->has('released_date'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->store_movie->first('released_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="saveBtn">Save</button>

        </form>
        @endslot
        @slot('footer') @endslot

    @endcomponent

    {{-- View Modal Component --}}
    @component('components.modal')
        @slot('id') viewMovieModal @endslot
        @slot('title') View Movie @endslot
        @slot('body')
        <div>
            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>

                <div class="col-md-6">
                    <input id="view_title" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="genre" class="col-md-4 col-form-label text-md-right">Genre</label>

                <div class="col-md-6">
                    <input id="view_genre" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="released_date" class="col-md-4 col-form-label text-md-right">Released Date</label>

                <div class="col-md-6">
                    <input id="view_released_date" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="is_available" class="col-md-4 col-form-label text-md-right">Released Date</label>

                <div class="col-md-6">
                    <input id="view_is_available" type="text" class="form-control" readonly>
                </div>
            </div>
        </div>
        @endslot
        @slot('footer') @endslot

    @endcomponent

    {{-- Edit Modal Component --}}
    @component('components.modal')
        @slot('id') editMovieModal @endslot
        @slot('title') Edit Movie @endslot
        @slot('body')
        <form id="edit_form" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>

                <div class="col-md-6">
                    <input id="edit_title" type="text" class="form-control{{ $errors->update_movie->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" placeholder="Title" required>
                    @if ($errors->update_movie->has('title'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->update_movie->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="genre" class="col-md-4 col-form-label text-md-right">Genre</label>

                <div class="col-md-6">
                    <select id="edit_genre" type="text" class="form-control{{ $errors->update_movie->has('genre') ? ' is-invalid' : '' }}" name="genre" value="{{ old('genre') }}" required>
                        <option value="Action">Action</option>
                        <option value="Horror">Horror</option>
                        <option value="Thriller">Thriller</option>
                        <option value="Romance">Romance</option>
                    </select>
                    @if ($errors->update_movie->has('genre'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->update_movie->first('genre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="released_date" class="col-md-4 col-form-label text-md-right">Released Date</label>

                <div class="col-md-6">
                    <input id="edit_released_date" type="date" class="form-control{{ $errors->update_movie->has('released_date') ? ' is-invalid' : '' }}" name="released_date" value="{{ old('released_date') }}" placeholder="Released Date" required>
                    @if ($errors->update_movie->has('released_date'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->update_movie->first('released_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="saveBtn">Save</button>

        </form>
        @endslot
        @slot('footer') @endslot

    @endcomponent
@endpush

@push('styles')
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/sweetalert2.css">
@endpush

@push('scripts')
    <script src="js/datatables.js"></script>
    <script src="js/sweetalert2.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize movies datatable
            $('#movies').DataTable();

            // Sweet alert component
            @component('components.sw2')
                @slot('title')
                    {!! session('title') !!}
                @endslot
                @slot('description')
                    {!! session('description') !!}
                @endslot
                @slot('status')
                    {!! session('status') !!}
                @endslot
            @endcomponent

            $(document).on("click", "#viewMovieBtn", function () {
                var title = $(this).data('title');
                var genre = $(this).data('genre');
                var released_date = $(this).data('released-date');

                $('#view_title').val(title);
                $('#view_genre').val(genre);
                $('#view_released_date').val(released_date);
            });

            $(document).on("click", "#editMovieBtn", function () {
                var action = $(this).data('action');
                var title = $(this).data('title');
                var genre = $(this).data('genre');
                var released_date = $(this).data('released-date');

                $('#edit_form').attr('action', action);
                $('#edit_title').val(title);
                $('#edit_genre').val(genre);
                $('#edit_released_date').val(released_date);
            });

            $(document).on("click", "#deleteMovieBtn", function () {
                var delCtx = $(this);
                var action = delCtx.data('action');

                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    html:
                        "<form id='delForm' method='POST' action='"+action+"'>" +
                        "<input type='hidden' name='_token' value='"+$('meta[name="csrf-token"]').attr('content')+"'>" +
                        "<input type='hidden' name='_method' value='DELETE'>" +
                        "</form>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $('#delForm').submit();
                    }
                });
            });

            @if (count($errors->store_movie) > 0)
                $('#addMovieBtn').click();
            @endif

            @if (count($errors->update_member) > 0)
                setTimeout(function(){
                    $('#editMovieBtn[data-id="{{ session('id') }}"]').click();
                }, 500);
            @endif

        });
    </script>
@endpush