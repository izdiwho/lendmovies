@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Movies Available For Lending
                    </div>
                    <div class="card-body">
                        <table id="movies" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Genre</th>
                                    <th>Released Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($movies as $movie)
                                    <tr>
                                        <td>{{ $movie->title }}</td>
                                        <td>{{ $movie->genre }}</td>
                                        <td>{{ $movie->released_date }}</td>
                                        <td>
                                            <button
                                            id="lendMovieBtn"
                                            class="btn btn-sm btn-block"
                                            data-toggle="modal"
                                            data-target="#lendMovieModal"
                                            data-id="{{ $movie->id }}"
                                            data-title="{{ $movie->title }}">
                                            Lend</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('modals')

    {{-- Lend Movie Modal Component --}}
    @component('components.modal')
        @slot('id') lendMovieModal @endslot
        @slot('title') Lend this Movie @endslot
        @slot('body')
        <form action="{{ route('lending.store') }}" method="POST">
            {{ csrf_field() }}
            <input id="movie_id" type="hidden" name="movie_id">

            <div class="form-group row">
                <label for="movie_title" class="col-md-4 col-form-label text-md-right">Movie Title</label>

                <div class="col-md-6">
                    <input id="movie_title" type="text" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="member_id" class="col-md-4 col-form-label text-md-right">Member</label>

                <div class="col-md-6">
                    <select id="member_id" name="member_id">
                        @forelse ($members as $member)
                            <option value='{{ $member->id }}'>{{ $member->name }}</option>
                        @empty
                            No option found
                        @endforelse
                    </select>
                    @if ($errors->store_lending->has('member_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->store_lending->first('member_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="lending_date" class="col-md-4 col-form-label text-md-right">Lending Date</label>

                <div class="col-md-6">
                    <input id="lending_date" type="date" class="form-control{{ $errors->store_lending->has('lending_date') ? ' is-invalid' : '' }}" name="lending_date" value="{{ old('lending_date') }}" placeholder="Lending Date" required>
                    @if ($errors->store_lending->has('lending_date'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->store_lending->first('lending_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-block btn-primary col-md-6 offset-md-4" id="submitBtn">Submit</button>

        </form>
        @endslot
        @slot('footer') @endslot

    @endcomponent

@endpush

@push('styles')
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/sweetalert2.css">
    <link rel="stylesheet" href="css/multiselect.css">
@endpush

@push('scripts')
    <script src="js/datatables.js"></script>
    <script src="js/sweetalert2.js"></script>
    <script src="js/multiselect.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize movies datatable
            $('#movies').DataTable();

            // Sweet alert component
            @component('components.sw2')
                @slot('title')
                    {!! session('title') !!}
                @endslot
                @slot('description')
                    {!! session('description') !!}
                @endslot
                @slot('status')
                    {!! session('status') !!}
                @endslot
            @endcomponent

            // Activate Multiselect
            $('#member_id').multiselect({
                templates: {
                    li: '<li><a class="dropdown-item"><label class="m-0 pl-2 pr-0"></label></a></li>',
                    ul: ' <ul class="multiselect-container dropdown-menu p-1 m-0"></ul>',
                    button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" data-flip="false"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
                    filter: '<li class="multiselect-item filter"><div class="input-group m-0"><input class="form-control multiselect-search" type="text"></div></li>',
                    filterClearBtn: '<span class="input-group-btn"><button type="button" class="btn btn-secondary multiselect-clear-filter">&times;</button></span>'
                },
                buttonContainer: '<div class="dropdown" />',
                buttonClass: 'btn btn-secondary',
                enableCaseInsensitiveFiltering: true
            });

            $(document).on("click", "#lendMovieBtn", function () {
                var movie_id = $(this).data('id');
                var movie_title = $(this).data('title');

                $('#movie_id').val(movie_id);
                $('#movie_title').val(movie_title);
            });

            @if (count($errors->store_lending) > 0)
                setTimeout(function(){
                    $('#lendMovieBtn[data-id="{{ session('id') }}"]').click();
                }, 500);
            @endif

        });
    </script>
@endpush